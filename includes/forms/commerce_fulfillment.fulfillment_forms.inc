<?php
/**
 * @file
 * Forms for fulfilling orders.
 */

/**
 * Package form page callback.
 */
function commerce_fulfillment_package_content($form, &$form_state, $order) {
  $form = array();

  $form['#attributes'] = array(
    'id' => 'commerce-fulfillment-package-content',
    'class' => 'commerce-fulfillment-content'
  );

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'commerce_fulfillment') . '/css/commerce_fulfillment.css',
  );

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'commerce_fulfillment') . '/js/commerce_fulfillment.js',
  );

  $form['#attached']['library'] = array(
    array('system', 'ui.core'),
    array('system', 'ui.widget'),
    array('system', 'ui.mouse'),
    array('system', 'ui.draggable'),
    array('system', 'ui.droppable'),
    array('system', 'ui.dialog'),
    array('system', 'ui.position'),
    array('system', 'ui.button'),
  );

  // Hidden select used for DnD.
  $form['commerce_fulfillment_dnd'] = array(
    '#type' => 'submit',
    '#value' => 'DnD',
    '#submit' => array('commerce_fulfillment_package_content_dnd'),
    '#attributes' => array(
      'id' => 'edit-commerce-fulfillment-dnd',
    ),
    '#ajax' => array(
      'callback' => 'commerce_fulfillment_content_callback',
      'wrapper' => 'commerce-fulfillment-package-content',
    ),
  );

  // Stores the product id of the item dragged.
  $form['commerce_fulfillment_product_id'] = array(
    '#type' => 'hidden',
  );

  // Stores the package item id of the item dragged (-1 if no package item yet).
  $form['commerce_fulfillment_package_item_id'] = array(
    '#type' => 'hidden',
  );

  // Stores the package id of the drop location (-1 if removing).
  $form['commerce_fulfillment_package_id'] = array(
    '#type' => 'hidden',
  );

  // Stores the quantity of the item dragged.
  $form['commerce_fulfillment_quantity'] = array(
    '#type' => 'hidden',
  );

  // Stores the order id for this order.
  $form['commerce_fulfillment_order_id'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );

  $form['commerce_fulfillment_package_type'] = array(
    '#type' => 'select',
    '#title' => t('Create a new package:'),
    '#options' => _commerce_fulfillment_package_types_options(),
    '#default_value' => 'default',
  );

  // Display all line items.
  $form['commerce_fulfillment_line_items'] = array(
    '#theme' => 'commerce_fulfillment_line_items',
  );
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $unallocated_product_ids = array();
  foreach ($order_wrapper->commerce_line_items->value() as $key => $line_item) {
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    if (isset($line_item_wrapper->value()->type)) {
      if ($line_item_wrapper->value()->type === 'product') {
        $product_id = $line_item_wrapper->commerce_product->getIdentifier();
        $quantity = _commerce_fulfillment_get_unpackaged_quantity($order->order_id, $product_id);
        if ($quantity > 0) {
          if (!in_array($product_id, $unallocated_product_ids)) {
            // Only generate the unallocated product once per product, as the
            // line items are combined by product id.
            $unallocated_product_ids[] = $product_id;
            $product = $line_item_wrapper->commerce_product->value();
            $form['commerce_fulfillment_line_items'][$key] = _commerce_fulfillment_generate_line_item_form(-1, $product, $quantity, TRUE);
          }
        }
      }
    }
  }

  // Add a remove all checkbox to header.
  $form['commerce_fulfillment_remove_all'] = array(
    '#type' => 'checkbox',
    '#value' => 0,
  );

  // Display all packages.
  $packages = commerce_fulfillment_package_load_by_order($order->order_id);
  foreach ($packages as $key=>$package) {
    $form['commerce_fulfillment_packages'][$key] = _commerce_fulfillment_generate_package_form($package);
  }

  $create_packages_classes = array();
  if (count($packages) === 0) {
    $create_packages_classes[] = 'highlight';
    $form['commerce_fulfillment_create_hint'] = array(
      '#prefix' => '<div class="hint">',
      '#markup' => t('Create a package to get started'),
      '#suffix' => '</div>',
    );
  }
  $form['commerce_fulfillment_create'] = array(
    '#type' => 'submit',
    '#value' => t('Create a Package'),
    '#submit' => array('commerce_fulfillment_package_content_create'),
    '#ajax' => array(
      'callback' => 'commerce_fulfillment_content_callback',
      'wrapper' => 'commerce-fulfillment-package-content',
    ),
    '#attributes' => array(
      'class' => $create_packages_classes,
    ),
  );

  $form['commerce_fulfillment_delete'] = array(
    '#type' => 'submit',
    '#value' => 'Remove Package(s)',
    '#submit' => array('commerce_fulfillment_package_content_remove'),
    '#ajax' => array(
      'callback' => 'commerce_fulfillment_content_callback',
      'wrapper' => 'commerce-fulfillment-package-content',
    ),
  );

  return $form;
}

/**
 * Returns the form section for a line item.
 *
 * @param $package_item_id
 *  The package item id for the line item, -1 if does not have a package item.
 * @param $product
 *  The loaded product to create the line item from.
 * @param $quantity
 *  The number of these products in the line item.
 * @param $unallocated
 *  Whether this line item is already allocated or not
 * @return array
 *  The form section for that line item.
 */
function _commerce_fulfillment_generate_line_item_form($package_item_id, $product, $quantity, $unallocated) {
  $quantity = floor($quantity);
  $product_wrapper = entity_metadata_wrapper('commerce_product', $product);

  $form = array(
    '#type' => 'fieldset',
    '#data-commercefulfillment-product-id' => $product_wrapper->getIdentifier(),
    '#data-commercefulfillment-package-item-id' => $package_item_id,
    '#data-commercefulfillment-package-item-quantity' => $quantity,
  );

  $form['sku'] = array(
    '#markup' => $product_wrapper->sku->value(),
  );

  $form['title'] = array(
    '#markup' => $product_wrapper->title->value(),
  );

  $unit_price = $product_wrapper->commerce_price->value();
  $form['unit_price'] = array(
    '#markup' => commerce_currency_format($unit_price['amount'], $unit_price['currency_code']),
  );

  if ($unallocated) {
    $form['quantity'] = array(
      '#markup' => $quantity,
    );
  } else {
    $form['quantity'] = array(
      '#type' => 'textfield',
      '#value' => $quantity,
      '#size' => 1,
    );
  }

  $total_price = $unit_price['amount'] * $quantity;
  $form['total_price'] = array(
    '#markup' => commerce_currency_format($total_price, $unit_price['currency_code']),
  );
  return $form;
}

/**
 * Returns the form section for a package.
 *
 * @param $package
 *  The package to create the form section for.
 * @param $simple
 *  If true, generates line items as simple non-draggable items.
 * @return array
 *  The form section for that package.
 */
function _commerce_fulfillment_generate_package_form($package, $simple = FALSE) {
  $package_wrapper = entity_metadata_wrapper('commerce_fulfillment_package', $package);

  $id = $package_wrapper->getIdentifier();
  $form = array(
    '#type' => 'fieldset',
    '#data-commercefulfillment-package-id' => $id,
  );

  $is_empty = _commerce_fulfillment_package_is_empty($package);
  // Stores if the item should be disabled from Drag and drop on shipment form.
  $form['disabled'] = array(
    '#type' => 'hidden',
    '#value' => $is_empty,
  );

  $form['remove_' . $id] = array(
    '#type' => 'checkbox',
  );

  $form['id'] = array(
    '#markup' => $id,
  );

  $tax_id = $package_wrapper->box_type->value();
  $term = taxonomy_term_load($tax_id);
  $form['type'] = array(
    '#markup' => $term ? $term->name : 'Error: Package type deleted or not found',
  );

  if ($simple) {
    $package_items = $package_wrapper->commerce_package_items->value();
    foreach ($package_items as $key => $package_item) {
      $product = commerce_product_load($package_item->product_id);
      $form['line_items'][$key] = array(
        '#markup' => $product->sku,
        '#prefix' => '<div class="product-sku-list">',
        '#suffix' => '</div>',
      );
    }
  } else {
    $form['line_items'] = array(
      '#theme' => 'commerce_fulfillment_line_items',
    );
    $package_items = $package_wrapper->commerce_package_items->value();
    foreach ($package_items as $key => $package_item) {
      $product = commerce_product_load($package_item->product_id);
      $quantity = $package_item->quantity;
      $form['line_items'][$key] = _commerce_fulfillment_generate_line_item_form($package_item->package_item_id, $product, $quantity, FALSE);
    }
  }

  $link_classes = array('overlay-exclude');
  if ($is_empty) {
    $link_classes[] = 'disabled';
  }
  $form['operations'] = array(
    '#theme' => 'link',
    '#text' => 'Print Packing Slip',
    '#path' => '/admin/commerce/orders/commerce_fulfillment/packing_slip/' . $id,
    '#disabled' => $is_empty,
    '#options' => array(
      'html' => FALSE,
      'attributes' => array(
        'class' => $link_classes,
        'target' => '_blank',
      )
    ),
  );
  return $form;
}

/**
 * Drag and Drop submit function for commerce_fulfillment_package_content.
 */
function commerce_fulfillment_package_content_dnd($form, &$form_state) {
  $values = $form_state['values'];
  $product_id = $values['commerce_fulfillment_product_id'];
  $target_package_id = $values['commerce_fulfillment_package_id'];
  $quantity = $values['commerce_fulfillment_quantity'];
  $order_id = $values['commerce_fulfillment_order_id'];
  $package_item_id = $values['commerce_fulfillment_package_item_id'];
  commerce_fulfillment_move_product_quantity($order_id, $product_id, $package_item_id, $target_package_id, $quantity);

  $form_state['rebuild'] = TRUE;
}

/**
 * Create packages submit function for commerce_fulfillment_package_content.
 */
function commerce_fulfillment_package_content_create($form, &$form_state) {
  $order_id = $form_state['values']['commerce_fulfillment_order_id'];

  // Create the package entity.
  $entity = entity_create('commerce_fulfillment_package', array(
    'order_id' => $order_id,
    'box_type' => $form_state['values']['commerce_fulfillment_package_type'],
    'commerce_package_items' => NULL,
  ));

  $entity->save('commerce_fulfillment_package', $entity);

  $form_state['rebuild'] = TRUE;
}

/**
 * Remove packages submit function for commerce_fulfillment_package_content.
 */
function commerce_fulfillment_package_content_remove($form, &$form_state) {
  $keys = element_children($form['commerce_fulfillment_packages']);
  if (!empty($keys)) {
    foreach ($keys as $key) {
      if ($form_state['values']['remove_' . $key] === 1) {
        $package = commerce_fulfillment_package_load($key);
        if ($package) {
          _commerce_fulfillment_remove_package_from_shipments($package);
          _commerce_fulfillment_remove_package_items_from_package($package);
          entity_delete('commerce_fulfillment_package', $package->package_id);
        }
      }
    }
  } else {
    drupal_set_message('Please select a package to remove.', 'error');
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Shipment form page callback.
 */
function commerce_fulfillment_shipment_content($form, &$form_state, $order) {
  $form = array();

  $form['#attributes'] = array(
    'id' => 'commerce-fulfillment-shipment-content',
    'class' => 'commerce-fulfillment-content'
  );

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'commerce_fulfillment') . '/css/commerce_fulfillment.css',
  );

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'commerce_fulfillment') . '/js/commerce_fulfillment.js',
  );

  $form['#attached']['library'] = array(
    array('system', 'ui.core'),
    array('system', 'ui.widget'),
    array('system', 'ui.mouse'),
    array('system', 'ui.draggable'),
    array('system', 'ui.droppable'),
    array('system', 'ui.dialog'),
    array('system', 'ui.position'),
    array('system', 'ui.button'),
  );

  // Hidden select used for DnD.
  $form['commerce_fulfillment_dnd'] = array(
    '#type' => 'submit',
    '#value' => 'DnD',
    '#submit' => array('commerce_fulfillment_shipment_content_dnd'),
    '#attributes' => array(
      'id' => 'edit-commerce-fulfillment-dnd',
    ),
    '#ajax' => array(
      'callback' => 'commerce_fulfillment_content_callback',
      'wrapper' => 'commerce-fulfillment-shipment-content',
    ),
  );

  // Stores the order id.
  $form['commerce_fulfillment_order_id'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );

  // Stores the package id of the item dragged.
  $form['commerce_fulfillment_package_id'] = array(
    '#type' => 'hidden',
  );

  // Stores the shipment id of the current owner of the package.
  $form['commerce_fulfillment_shipment_id'] = array(
    '#type' => 'hidden',
  );

  // Stores the shipment id of the drop location (-1 if removing).
  $form['commerce_fulfillment_target_shipment_id'] = array(
    '#type' => 'hidden',
  );

  $form['commerce_fulfillment_new_tracking_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter a tracking number:'),
    '#size' => 20,
  );

  // Display all unallocated packages.
  $form['commerce_fulfillment_packages'] = array(
    '#theme' => 'commerce_fulfillment_packages',
  );

  // Find all packages that are not assigned to a shipment
  $shipments = commerce_fulfillment_shipment_load_by_order($order->order_id);
  $assigned_packages = array();
  foreach ($shipments as $shipment) {
    $shipment_wrapper = entity_metadata_wrapper('commerce_fulfillment_shipment', $shipment);
    $shipment_ids = $shipment_wrapper->commerce_fulfillment_packages->raw();
    $assigned_packages = array_merge($assigned_packages, $shipment_ids);
  }

  $packages = commerce_fulfillment_package_load_by_order($order->order_id);
  foreach ($packages as $key=>$package) {
    if (!in_array($package->package_id, $assigned_packages)) {
      $form['commerce_fulfillment_packages'][$key] = _commerce_fulfillment_generate_package_form($package, TRUE);
    }
  }

  // Add a remove all checkbox to header.
  $form['commerce_fulfillment_remove_all'] = array(
    '#type' => 'checkbox',
    '#value' => 0,
  );

  // Display all shipments.
  foreach ($shipments as $key=>$shipment) {
    $form['commerce_fulfillment_shipments'][$key] = _commerce_fulfillment_generate_shipment_form($shipment);
  }

  $create_shipment_classes = array();
  if (count($shipments) === 0) {
    $create_shipment_classes[] = 'highlight';
    $form['commerce_fulfillment_create_hint'] = array(
      '#prefix' => '<div class="hint">',
      '#markup' => t('Create a shipment to get started'),
      '#suffix' => '</div>',
    );
  }
  $form['commerce_fulfillment_create'] = array(
    '#type' => 'submit',
    '#value' => t('Create a Shipment'),
    '#submit' => array('commerce_fulfillment_shipment_content_create'),
    '#ajax' => array(
      'callback' => 'commerce_fulfillment_content_callback',
      'wrapper' => 'commerce-fulfillment-shipment-content',
    ),
    '#attributes' => array(
      'class' => $create_shipment_classes,
    ),
  );

  $form['commerce_fulfillment_delete'] = array(
    '#type' => 'submit',
    '#value' => 'Remove Shipment(s)',
    '#submit' => array('commerce_fulfillment_shipment_content_remove'),
    '#ajax' => array(
      'callback' => 'commerce_fulfillment_content_callback',
      'wrapper' => 'commerce-fulfillment-shipment-content',
    ),
  );

  return $form;
}

/**
 * Returns the form section for a shipment.
 *
 * @param $shipment
 *  The shipment to generate the form for.
 * @return array
 *  The form section for that shipment.
 */
function _commerce_fulfillment_generate_shipment_form($shipment) {
  $shipment_wrapper = entity_metadata_wrapper('commerce_fulfillment_shipment', $shipment);

  $id = $shipment_wrapper->getIdentifier();
  $form = array(
    '#type' => 'fieldset',
    '#data-commercefulfillment-shipment-id' => $id,
  );

  $form['remove_' . $id] = array(
    '#type' => 'checkbox',
  );

  $form['tracking_number'] = array(
    '#markup' => $shipment_wrapper->tracking_number->value(),
  );

  $form['status'] = array(
    '#markup' => $shipment_wrapper->status->value(),
  );

  $form['packages'] = array(
    '#theme' => 'commerce_fulfillment_packages',
  );
  $packages = $shipment_wrapper->commerce_fulfillment_packages->value();
  foreach ($packages as $key=>$package) {
    $package = commerce_fulfillment_package_load($package->package_id);
    $form['packages'][$key] = _commerce_fulfillment_generate_package_form($package, TRUE);
  }

  $link_classes = array('overlay-exclude');
  if (empty($packages)) {
    $link_classes[] = 'disabled';
  }
  $form['operations'] = array(
    '#theme' => 'link',
    '#text' => 'Print Shipping Label',
    '#path' => '/admin/commerce/orders/commerce_fulfillment/shipping_label/' . $id,
    '#options' => array(
      'html' => FALSE,
      'attributes' => array(
        'class' => $link_classes,
        'target' => '_blank',
      )
    ),
  );
  return $form;
}

/**
 * Drag and Drop submit function for commerce_fulfillment_shipment_content.
 */
function commerce_fulfillment_shipment_content_dnd($form, &$form_state) {
  $values = $form_state['values'];
  $package_id = $values['commerce_fulfillment_package_id'];
  $shipment_id = $values['commerce_fulfillment_shipment_id'];
  $target_shipment_id = $values['commerce_fulfillment_target_shipment_id'];

  commerce_fulfillment_move_package($package_id, $shipment_id, $target_shipment_id);

  $form_state['rebuild'] = TRUE;
}

/**
 * Create shipments submit function for commerce_fulfillment_shipment_content.
 */
function commerce_fulfillment_shipment_content_create($form, &$form_state) {
  $order_id = $form_state['values']['commerce_fulfillment_order_id'];

  $entity = entity_create('commerce_fulfillment_shipment', array(
    'order_id' => $order_id,
    'status' => 'Pending',
    'tracking_number' => $form_state['values']['commerce_fulfillment_new_tracking_number'],
  ));

  $entity->save('commerce_fulfillment_shipment', $entity);

  $form_state['rebuild'] = TRUE;
}

/**
 * Remove shipments submit function for commerce_fulfillment_shipment_content.
 */
function commerce_fulfillment_shipment_content_remove($form, &$form_state) {
  $keys = element_children($form['commerce_fulfillment_shipments']);
  if (!empty($keys)) {
    foreach ($keys as $key) {
      if ($form_state['values']['remove_' . $key] === 1) {
        $shipment = commerce_fulfillment_shipment_load($key);
        if ($shipment) {
          entity_delete('commerce_fulfillment_shipment', $shipment->shipment_id);
        }
      }
    }
  } else {
    drupal_set_message('Please select a shipment to remove.', 'error');
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback to replace entire form.
 */
function commerce_fulfillment_content_callback($form, &$form_state) {
  return $form;
}
