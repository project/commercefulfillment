/**
 * @file
 * commerce_fulfillment.js
 *
 *  Custom JavaScript for Commerce Fulfillment drag and drop.
 */

(function ($) {

  $(function() {
    $('.quantity-popup').dialog({
      autoOpen: false,
      dialogClass: 'ui-dialog-quantity-popup',
      width: 140,
      minWidth: 140,
      minHeight: 10,
      buttons: [
        {
          text: 'Move',
          click: function () {
            var quantity = $('.ui-dialog-quantity-popup input').val();
            $('.ui-dialog-quantity-popup input').val('');
            $('input[name=commerce_fulfillment_quantity]').attr('value', quantity);
            $('#edit-commerce-fulfillment-dnd').mousedown();
            $(this).dialog('close');
            commerce_fulfillment_clear_package_info();
          }
        }
      ]
    });

    // Make enter submit quantity popup.
    $('.ui-dialog-quantity-popup .quantity-popup input').keypress(function (event) {
      if (event.which == 13) {
        $('.ui-dialog-quantity-popup .ui-button').click();
      }
    });
  });

  Drupal.behaviors.commerce_fulfillment = {
    'attach': function(context) {
      $('.row-item-enabled', context).draggable({
        zIndex: 100,
        revert: 'invalid',
        revertDuration: 0,
        helper: 'clone',
        containment: '.commerce-fulfillment-content',
        start: function(event, ui) {
          $(this).addClass('selected');
        },
        stop: function(event, ui) {
          $(this).removeClass('selected');
        }
      });

      /* -- Packages Form Drag and Drop -- */

      $('.unused-items-line-items', context).droppable({
        hoverClass: 'drop-hover',
        accept: '.table-containers .row-item-enabled',
        drop: function (event, ui) {
          var draggable = ui.draggable;
          var product_id = $(draggable).attr('data-commercefulfillment-product-id');
          var package_item_id = $(draggable).attr('data-commercefulfillment-package-item-id');
          var quantity = $(draggable).attr('data-commercefulfillment-package-item-quantity');
          var package_id = -1;
          // Only open dialog if move than 1 quantity.
          if (quantity > 1) {
            commerce_fulfillment_set_package_info(product_id, package_item_id, package_id);
            $('.quantity-popup').closest('div.ui-dialog-content').dialog('open');
          } else {
            commerce_fulfillment_set_package_info(product_id, package_item_id, package_id, quantity);
            $('#edit-commerce-fulfillment-dnd').mousedown();
            commerce_fulfillment_clear_package_info();
          }
        }
      });

      $('.table-containers .row-container .field-line-items', context).droppable({
        hoverClass: 'drop-hover',
        accept: function (draggable) {
          var draggable_parent = $(draggable).parents('.field-line-items');
          return this !== draggable_parent.get(0);
        },
        drop: function (event, ui) {
          var draggable = ui.draggable;
          var product_id = $(draggable).attr('data-commercefulfillment-product-id');
          var package_item_id = $(draggable).attr('data-commercefulfillment-package-item-id');
          var quantity = $(draggable).attr('data-commercefulfillment-package-item-quantity');
          var package_id = $(this).closest('.row-container').attr('data-commercefulfillment-package-id');
          // Only open dialog if move than 1 quantity.
          if (quantity > 1) {
            commerce_fulfillment_set_package_info(product_id, package_item_id, package_id);
            $('.quantity-popup').closest('div.ui-dialog-content').dialog('open');
          } else {
            commerce_fulfillment_set_package_info(product_id, package_item_id, package_id, quantity);
            $('#edit-commerce-fulfillment-dnd').mousedown();
            commerce_fulfillment_clear_package_info();
          }
        }
      });

      /* -- Shipment Form Drag and Drop -- */

      $('.unused-items-packages', context).droppable({
        hoverClass: 'drop-hover',
        accept: '.table-containers .row-item-enabled',
        drop: function (event, ui) {
          var draggable = ui.draggable;
          var package_id = $(draggable).attr('data-commercefulfillment-package-id');
          var shipment_id = $(draggable).closest('.row-container').attr('data-commercefulfillment-shipment-id');
          var target_shipment_id = -1;
          commerce_fulfillment_set_shipment_info(package_id, shipment_id, target_shipment_id);
          $('#edit-commerce-fulfillment-dnd').mousedown();
          commerce_fulfillment_clear_shipment_info();
        }
      });

      $('.table-containers .row-container .field-packages', context).droppable({
        hoverClass: 'drop-hover',
        accept: function (draggable) {
          var draggable_parent = $(draggable).parents('.field-packages');
          return this !== draggable_parent.get(0);
        },
        drop: function (event, ui) {
          var draggable = ui.draggable;
          var package_id = $(draggable).attr('data-commercefulfillment-package-id');
          var shipment_id = $(draggable).closest('.row-container').attr('data-commercefulfillment-shipment-id');
          var target_shipment_id = $(this).closest('.row-container').attr('data-commercefulfillment-shipment-id');
          commerce_fulfillment_set_shipment_info(package_id, shipment_id, target_shipment_id);
          $('#edit-commerce-fulfillment-dnd').mousedown();
          commerce_fulfillment_clear_shipment_info();
        }
      });

      // Make the select all checkbox select all other checkboxes.
      $('.form-item-commerce-fulfillment-remove-all input').click(function() {
        if ($(this).attr('checked')) {
          $('.field-remove .form-checkbox').attr('checked', true);
        } else {
          $('.field-remove .form-checkbox').attr('checked', false);
        }
      });

      // Submit proper values for quantity change
      $('.row-item-enabled .field-quantity input').blur(function() {
        var product_id, package_item_id, package_id;

        var quantity = $(this).closest('.row-item-enabled').attr('data-commercefulfillment-package-item-quantity');
        var quantity_change = Math.floor($(this).val()) - quantity;
        if (quantity_change > 0) {
          // Move quantity from allocated to package.
          product_id = $(this).closest('.row-item-enabled').attr('data-commercefulfillment-product-id');
          package_item_id = -1;
          package_id = $(this).closest('.row-container').attr('data-commercefulfillment-package-id');
        } else if (quantity_change < 0) {
          // Move quantity from package item to unallocated.
          product_id = $(this).closest('.row-item-enabled').attr('data-commercefulfillment-product-id');
          package_item_id = $(this).closest('.row-item-enabled').attr('data-commercefulfillment-package-item-id');
          package_id = -1;
        } else {
          // Do nothing if no quantity change
          return;
        }
        quantity_change = Math.abs(quantity_change);
        commerce_fulfillment_set_package_info(product_id, package_item_id, package_id, quantity_change);
        $('#edit-commerce-fulfillment-dnd').mousedown();
        commerce_fulfillment_clear_package_info();
      });
    }
  };

  function commerce_fulfillment_set_package_info(product_id, package_item_id, package_id, quantity) {
    if (product_id === undefined) {
      product_id = -1;
    }
    if (package_item_id === undefined) {
      package_item_id = -1;
    }
    if (package_id === undefined) {
      package_id = -1;
    }
    $('input[name=commerce_fulfillment_product_id]').attr('value', product_id);
    $('input[name=commerce_fulfillment_package_item_id]').attr('value', package_item_id);
    $('input[name=commerce_fulfillment_package_id]').attr('value', package_id);
    if (quantity !== undefined) {
      $('input[name=commerce_fulfillment_quantity]').attr('value', quantity);
    }
  }

  function commerce_fulfillment_clear_package_info() {
    $('input[name=commerce_fulfillment_product_id]').attr('value', '');
    $('input[name=commerce_fulfillment_package_item_id]').attr('value', '');
    $('input[name=commerce_fulfillment_package_id]').attr('value', '');
    $('input[name=commerce_fulfillment_quantity]').attr('value', '');
  }

  function commerce_fulfillment_set_shipment_info(package_id, shipment_id, target_shipment_id) {
    if (package_id === undefined) {
      package_id = -1;
    }
    if (shipment_id === undefined) {
      shipment_id = -1;
    }
    if (target_shipment_id === undefined) {
      target_shipment_id = -1;
    }
    $('input[name=commerce_fulfillment_package_id]').attr('value', package_id);
    $('input[name=commerce_fulfillment_shipment_id]').attr('value', shipment_id);
    $('input[name=commerce_fulfillment_target_shipment_id]').attr('value', target_shipment_id);
  }

  function commerce_fulfillment_clear_shipment_info() {
    $('input[name=commerce_fulfillment_package_id]').attr('value', '');
    $('input[name=commerce_fulfillment_shipment_id]').attr('value', '');
    $('input[name=commerce_fulfillment_target_shipment_id]').attr('value', '');
  }

})(jQuery);
