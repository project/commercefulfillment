<?php

/**
 * @file
 * Builds the two custom entities for the module.
 */

/**
 * Implements hook_schema().
 */
function commerce_fulfillment_schema() {

  $schema = array();

  $schema['commerce_fulfillment_packages'] = array(
    'description' => 'The base table for the package entity',
    'fields' => array(
      'package_id' => array(
        'description' => 'Primary key of the package entity',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'order_id' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'box_type' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('package_id'),
    'foreign keys' => array(
      'order_id' => array(
        'table' => 'commerce_order',
        'columns' => array('order_id' => 'order_id'),
      ),
      'commerce_fulfillment_package_items' => array(
        'table' => 'commerce_fulfillment_package_items',
        'columns' => array('package_item_id' => 'package_item_id'),
      ),
    ),
  );

  $schema['commerce_fulfillment_package_items'] = array(
    'description' => 'The base table for the Package Item entity',
    'fields' => array(
      'package_item_id' => array(
        'description' => 'Primary key of the Package Item entity',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'package_id' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'product_id' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'quantity' => array(
        'type' => 'int',
        'size' => 'normal',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('package_item_id'),
    'foreign keys' => array(
      'package_id' => array(
        'table' => 'commerce_fulfillment_packages',
        'columns' => array('package_id' => 'package_id'),
      ),
      'product_id' => array(
        'table' => 'commerce_product',
        'columns' => array('product_id' => 'product_id'),
      ),
    ),
  );

  $schema['commerce_fulfillment_shipments'] = array(
    'description' => 'The base table for the Shipment entity',
    'fields' => array(
      'shipment_id' => array(
        'description' => 'Primary key of the Shipment entity',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'order_id' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'Status of the shipment("Pending" or "Completed")',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
      'tracking_number' => array(
        'description' => 'Tracking number for the shipments',
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('shipment_id'),
    'foreign keys' => array(
      'order_id' => array(
        'table' => 'commerce_order',
        'columns' => array('order_id' => 'order_id'),
      ),
      'commerce_fulfillment_packages' => array(
        'table' => 'commerce_fulfillment_packages',
        'columns' => array('commerce_fulfillment_packages' => 'commerce_fulfillment_packages'),
      ),
    ),
  );

  return $schema;

}

/**
 * Implements hook_install().
 */
function commerce_fulfillment_install() {
  _commerce_fulfillment_install_package_type_vocabulary();
}

/**
 * Implements hook_enable().
 */
function commerce_fulfillment_enable() {
  commerce_fulfillment_configure_package_type();
  commerce_fulfillment_configure_shipment_type();
}

/**
 * Implements hook_uninstall().
 */
function commerce_fulfillment_uninstall() {
  variable_del('commerce_fulfillment_package_type');
  variable_del('commerce_fulfillment_company');
  variable_del('commerce_fulfillment_address');
  variable_del('commerce_fulfillment_phone');
  variable_del('commerce_fulfillment_boxtype');
  variable_del('commerce_fulfillment_logo');
  variable_del('commerce_fulfillment_ship_with_insurance');

  foreach (
    array(
      'commerce_package_items',
      'commerce_fulfillment_packages',
      'commerce_fulfillment_shipping',
      'commerce_fulfillment_line_items',
      'field_package_tracking_id',
      'field_package_label',
    ) as $field_name) {
    field_delete_field($field_name);
  }
}

/**
 * Ensures the package item field is present on the default package bundle.
 *
 * @param string $type
 *   Which entity the field will be attached to.
 */
function commerce_fulfillment_configure_package_type($type = 'commerce_fulfillment_package') {

  // Can not name 'commerce_fulfillment_package_items' due to 32 char limit.
  $field_name = 'commerce_package_items';
  field_cache_clear();

  $field = field_info_field($field_name);
  $instance = field_info_instance('commerce_fulfillment_package', $field_name, $type);

  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'commerce_package_items',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'entity_types' => array('commerce_fulfillment_package'),
      'settings' => array(
        'target_type' => 'commerce_fulfillment_package_item',
        'handler_settings' => array('target_bundles' => NULL),
      ),
      'translatable' => FALSE,
      'locked' => TRUE,
      'default_formatter' => 'commerce_fulfillment_package_item_reference_view',
    );
    field_create_field($field);
  }
  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => 'commerce_fulfillment_package',
      'bundle' => $type,
      'label' => t('Items in package'),
      'settings' => array(
        'target_type' => 'commerce_fulfillment_package_item',
        'handler_settings' => array('target_bundles' => NULL),
      ),
      'widget' => array(
        'type' => 'commerce_fulfillment_package_item_manager',
        'weight' => -10,
      ),
      'display' => array(),
    );

    // Set the default display formatters for various view modes.
    foreach (array('default', 'customer', 'administrator') as $view_mode) {
      $instance['display'][$view_mode] = array(
        'label' => 'hidden',
        'type' => 'commerce_fulfillment_package_item_reference_view',
        'weight' => -10,
      );
    }

    field_create_instance($instance);
  }


  // Add tracking number field
  $field_name = 'field_package_tracking_id';
  $field = field_info_field($field_name);
  $instance = field_info_instance('commerce_fulfillment_package', $field_name, $type);
  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'text',
      'cardinality' => 1,
      'entity_types' => array('commerce_fulfillment_package'),
    );
    field_create_field($field);
  }
  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'label' => t('Tracking Number'),
      'entity_type' => 'commerce_fulfillment_package',
      'bundle' => $type,
    );

    field_create_instance($instance);
  }

  // Add shipping label field
  $field_name = 'field_package_label';
  $field = field_info_field($field_name);
  $instance = field_info_instance('commerce_fulfillment_package', $field_name, $type);
  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'file',
      'cardinality' => 1,
      'entity_types' => array('commerce_fulfillment_package'),
    );
    field_create_field($field);
  }
  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'label' => t('Shipping Label'),
      'description' => t("This field stores a package's shipping label."),
      'entity_type' => 'commerce_fulfillment_package',
      'bundle' => $type,
      'settings' => array(
        'file_directory' => 'fulfillment/shipping_labels',
      ),
    );

    field_create_instance($instance);
  }
}

/**
 * Ensures the packages field is present on the default shipment bundle.
 *
 * @param string $type
 *   Which entity the field will be attached to.
 */
function commerce_fulfillment_configure_shipment_type($type = 'commerce_fulfillment_shipment') {

  // Add a field to reference packages in the shipment.
  $field_name = 'commerce_fulfillment_packages';

  field_cache_clear();

  $field = field_info_field($field_name);
  $instance = field_info_instance('commerce_fulfillment_shipment', $field_name, $type);

  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => 'commerce_fulfillment_packages',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'entity_types' => array('commerce_fulfillment_shipment'),
      'settings' => array(
        'target_type' => 'commerce_fulfillment_package',
        'handler_settings' => array('target_bundles' => NULL),
      ),
      'translatable' => FALSE,
      'locked' => TRUE,
    );
    field_create_field($field);
  }

  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => 'commerce_fulfillment_shipment',
      'bundle' => $type,
      'label' => t('Packages'),
      'settings' => array(
        'target_type' => 'commerce_fulfillment_package',
        'handler_settings' => array('target_bundles' => NULL),
      ),
      'display' => array(),
    );

    // Set the default display formatters for various view modes.
    foreach (array('default', 'customer', 'administrator') as $view_mode) {
      $instance['display'][$view_mode] = array(
        'label' => 'hidden',
        'type' => 'commerce_fulfillment_package_reference_view',
        'weight' => -10,
      );
    }

    field_create_instance($instance);
  }
}

function _commerce_fulfillment_install_package_type_vocabulary() {

  // Initialize vocabulary.
  $vocabulary = new stdClass();
  $vocabulary->name = t('Package Type');
  $vocabulary->machine_name = 'package_type';
  $vocabulary->description = t('This taxonomy vocabulary is used by the commerce_fulfillment module to define package types.');
  $vocabulary->hierarchy = 0;
  $vocabulary->module = 'commerce_fulfillment';

  // Ensure no vocabulary with same machine name exists before saving.
  $existing_vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary->machine_name);
  if (empty($existing_vocabulary)) {
    taxonomy_vocabulary_save($vocabulary);

    // When developing without installing a shipping module that integrates
    // with commerce_fulfillment it may be helpful to install these example
    // package types.
    $default_terms = array();
    $default_terms['USPS'] = array(
      'usps_small_flat_rate' => 'Small Flat Rate Box',
      'usps_medium_flat_rate' => 'Medium Flat Rate Box',
      'usps_large_flat_Rate' => 'Large Flat Rate Box',
    );
    $default_terms['FedEx'] = array(
      'fedex_box_small' => 'FexEx Box - Small',
      'fedex_box_medium' => 'FexEx Box - Medium',
      'fedex_box_large' => 'FexEx Box - Large',
    );

    // Add terms to vocabulary.
    //_commerce_fulfillment_taxonomy_vocabulary_terms_install($vocabulary->vid, $default_terms);

  }

  // Add dimensions fields to terms.
  $fields = array(
    'field_package_length' => 'Package Length',
    'field_package_width' => 'Package Width',
    'field_package_height' => 'Package Height'
  );
  foreach ($fields as $field_name => $field_label) {
    $field = field_info_field($field_name);
    if (!$field) {
      $field = array(
        'field_name' => $field_name,
        'type' => 'text',
      );

      $field = field_create_field($field);

    }

    $instance = field_info_instance('taxonomy_term', $field_name, 'package_type');
    if (!$instance) {
      $instance = array(
        'field_name' => $field_name,
        'entity_type' => 'taxonomy_term',
        'bundle' => 'package_type',
        'label' => t('@field_label', array('@field_label' => $field_label)),
      );

      $instance = field_create_instance($instance);

    }
  }

  // Add unit field.
  $fields = array(
    'field_package_dimensions_unit' => t('Dimensions Unit'),
  );

  foreach ($fields as $field_name => $field_label) {

    $field = field_info_field($field_name);
    if (!$field) {

      $field = array(
        'field_name' => $field_name,
        'type' => 'list_text',
        'settings' => array(
          'allowed_values' => array(
            'in' => 'in',
            'ft' => 'ft',
            'cm' => 'cm',
            'mm' => 'mm',
            'm' => 'm',
          ),
        ),
      );

      $field = field_create_field($field);
    }

    $instance = field_info_instance('taxonomy_term', $field_name, 'package_type');
    if (!$instance) {

      $instance = array(
        'field_name' => $field_name,
        'entity_type' => 'taxonomy_term',
        'bundle' => 'package_type',
        'label' => $field_label,
        'description' => t('Units for package dimensions.'),
      );

      $instance = field_create_instance($instance);

    }
  }
}

/**
 * Helper function to install a default set of package types during development.
 * @param $vid
 * @param $terms
 */
function _commerce_fulfillment_taxonomy_vocabulary_terms_install($vid, $terms) {

  // Save parent terms.
  foreach ($terms as $group => $children) {

    // Save child terms.
    foreach ($children as $machine_name => $label) {
      $term = new stdClass();
      $term->vid = $vid;
      $term->name = $group . ' - ' . $label;
      taxonomy_term_save($term);
    }
  }
}

/**
 * - Install new required module commerce_fulfillment_field.
 * - Alter database schema.
 * - Add new fields to entities.
 */
function commerce_fulfillment_update_7000() {
  // Install new prerequisite: commerce_fulfillment_field.
  $dependencies = array('commerce_fulfillment_field');
  module_enable($dependencies);

  // Include all controller classes as they moved locations and drupal needs to
  // be notified of them.
  module_load_include('inc', 'commerce_fulfillment', 'includes/commerce_fulfillment.package_controller');
  module_load_include('inc', 'commerce_fulfillment', 'includes/commerce_fulfillment.package_entity');
  module_load_include('inc', 'commerce_fulfillment', 'includes/commerce_fulfillment.package_item_controller');
  module_load_include('inc', 'commerce_fulfillment', 'includes/commerce_fulfillment.package_item_entity');
  module_load_include('inc', 'commerce_fulfillment', 'includes/commerce_fulfillment.shipment_controller');
  module_load_include('inc', 'commerce_fulfillment', 'includes/commerce_fulfillment.shipment_entity');

  // Definitions for new schema.
  $schema = array();

  $table = 'commerce_fulfillment_package_items';
  if (!db_table_exists($table)) {
    $schema[$table] = array(
      'description' => 'The base table for the Package Item entity',
      'fields' => array(
        'package_item_id' => array(
          'description' => 'Primary key of the Package Item entity',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'package_id' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'product_id' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'quantity' => array(
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('package_item_id'),
      'foreign keys' => array(
        'package_id' => array(
          'table' => 'commerce_fulfillment_packages',
          'columns' => array('package_id' => 'package_id'),
        ),
        'product_id' => array(
          'table' => 'commerce_product',
          'columns' => array('product_id' => 'product_id'),
        ),
      ),
    );

    // Create new table commerce_fulfillment_package_items.
    db_create_table($table, $schema[$table]);
  }

  // Add fields to CommerceFulfillmentPackage entities, if necessary.
  commerce_fulfillment_configure_package_type();

  // Add fields to CommerceFulfillmentShipment entities, if necessary.
  commerce_fulfillment_configure_shipment_type();

  // Migrate old line items to new package items.
  $results = db_select('field_data_commerce_fulfillment_line_items', 'li')
    ->fields('li', array('entity_id', 'commerce_fulfillment_line_items_line_item_id'))
    ->condition('deleted', '0', '=')
    ->execute();
  foreach ($results as $result) {
    $line_item = commerce_line_item_load($result->commerce_fulfillment_line_items_line_item_id);
    if ($line_item) {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
      $product = $line_item_wrapper->commerce_product->value();
      // Create package_item entity
      $package_item = entity_create('commerce_fulfillment_package_item', array(
        'product_id' => $product->product_id,
        'quantity' => $line_item->quantity,
        'package_id' => $result->entity_id,
      ));
      entity_save('commerce_fulfillment_package_item', $package_item);

      // Add package item to package
      $package = entity_load('commerce_fulfillment_package', FALSE, array('package_id' => $result->entity_id));
      $package_wrapper = entity_metadata_wrapper('commerce_fulfillment_package', $package[$result->entity_id]);
      $package_wrapper->commerce_package_items[] = $package_item->package_item_id;
      $package_wrapper->save();
    }
  }
  field_delete_field('commerce_fulfillment_line_items');
}
