<?php
/**
 * @file
 * Template for the shipment form.
 */
?>
<div class="container-creation">
  <?php print render($form['commerce_fulfillment_dnd']); ?>
  <h2>Shipments:</h2>
  <?php print render($form['commerce_fulfillment_new_tracking_number']);
  print render($form['commerce_fulfillment_create']);
  print render($form['commerce_fulfillment_create_hint']); ?>
</div>
<div class="table table-unused-items">
  <div class="header">
    <div class="title">Unallocated Packages</div>
  </div>
  <?php $package_keys = element_children($form['commerce_fulfillment_packages']);
  if (!empty($package_keys)) { ?>
    <div class="description">
      <p>Drag a package into a shipment to add it.</p>
    </div>
  <?php } else { ?>
    <div class="description">
      <p>No packages to allocate.</p>
    </div>
  <?php } ?>
  <div class="unused-items unused-items-packages">
    <?php print render($form['commerce_fulfillment_packages'])?>
  </div>
  <div class="clear-both"></div>
</div>
<div id="shipments" class="table table-containers">
  <div class="header">
    <div class="field field-remove"><?php print render($form['commerce_fulfillment_remove_all']); ?></div>
    <div class="field field-tracking-number">Tracking Number</div>
    <div class="field field-status">Status</div>
    <div class="field field-shipping-operations">Operations</div>
    <div class="field field-packages">Package(s)</div>
  </div>
  <?php
  $shipments = isset($form['commerce_fulfillment_shipments'])? $form['commerce_fulfillment_shipments']: array();
  $shipment_keys = element_children($shipments);
  if (!empty($shipment_keys)) {
    $first = TRUE;
    foreach ($shipment_keys as $key) {
      $shipment = $form['commerce_fulfillment_shipments'][$key]; ?>
      <div id="shipment_<?php print $key; ?>" class="row row-container<?php ($first ? print ' first' : '') ?>" data-commercefulfillment-shipment-id="<?php print $shipment['#data-commercefulfillment-shipment-id'] ?>">
        <div class="field field-remove"><?php print render($shipment['remove_' . $key]); ?></div>
        <div class="field field-tracking-number"><?php print render($shipment['tracking_number']); ?></div>
        <div class="field field-status"><?php print render($shipment['status']); ?></div>
        <div class="field field-shipping-operations"><?php print render($shipment['operations']); ?></div>
        <div class="field field-packages"><?php print render($shipment['packages']); ?></div>
        <div class="clear-both"></div>
      </div>
      <?php
      $first = FALSE;
      hide($form['commerce_fulfillment_shipments'][$key]);
    }
  } else { ?>
    <div class="description">No shipments created.</div>
  <?php } ?>
</div>
<?php print render($form['commerce_fulfillment_delete']);
print drupal_render_children($form) ?>
