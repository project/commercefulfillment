<?php
/**
 * @file
 * Template for a shipment package.
 */
 ?>
<?php
$element = isset($element)? $element: array();
$package_keys = element_children($element);
if (!empty($package_keys)) { ?>
  <div id="packages" class="table table-items table-packages">
    <div class="header">
      <div class="field field-icon"></div>
      <div class="field field-id">Package ID</div>
      <div class="field field-type">Package Type</div>
      <div class="field field-operations">Operations</div>
      <div class="field field-line-items">Product(s)</div>
    </div>
  <?php
    $first = TRUE;
    foreach ($package_keys as $key) {
      $package = $element[$key];
      $disabled = $package['disabled']['#value']; ?>
      <div id="package_<?php print $key; ?>" class="row row-item<?php ($disabled ? print '-disabled' : print '-enabled'); ($first ? print ' first' : ''); ?>" data-commercefulfillment-package-id="<?php print $package['#data-commercefulfillment-package-id']; ?>">
        <div class="field field-icon"></div>
        <div class="field field-id"><?php print render($package['id']); ?></div>
        <div class="field field-type"><?php print render($package['type']); ?></div>
        <div class="field field-operations"><?php print render($package['operations']); ?></div>
        <div class="field field-line-items field-line-items-draggable"><?php print render($package['line_items']); ?></div>
        <div class="clear-both"></div>
      </div>
      <?php $first = FALSE;
    } ?>
    <div class="row-add-more row-add-more-attach">Drag items here.</div>
  </div>
<?php } else { ?>
  <div class="row-add-more row-add-more-new">Drag items here.</div>
<?php } ?>
