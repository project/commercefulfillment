<?php
/**
 * @file
 * Template for the packing slip.
 */
?>
<html>
<head>
  <title>Packing Slip <?php print $package_delta; ?> of <?php print $total_packages; ?></title>
  <?php print $page['css'] ?>
</head>
<body class="packing-slip">
<div id="packing-header">
  <div class="header-company">
    <?php if (!empty($image)): ?>
      <img src="<?php print $image; ?>" alt="<?php print $company; ?>"/>
    <?php endif; ?>

    <p class="company-info">
      <strong><?php print $company; ?></strong>
      <br><?php print $address; ?>
      <br><?php print $city; ?>, <?php print $state; ?>
      <br><?php print $zipcode; ?>
      <br>Phone: <?php print $phone; ?>
    </p>

    <p class="slip-head">
      Order: <?php print $order_number; ?>
      <br/>
      Date: <?php print date('Y/m/d'); ?>
    </p>
  </div>
</div>
<div class="customer-info">
  <h3 class="customer-label">Ship To:</h3>

  <p>
    <?php if (!empty($to_address)): ?>
      <?php print $to_address['name_line']; ?>
      <br/>
      <?php print $to_address['thoroughfare']; ?>
      <br/>
      <?php print $to_address['locality']; ?>
      <br/>
      <?php print $to_address['postal_code']; ?>
      <br/>
      <?php print $to_address['administrative_area']; ?>
    <?php endif; ?>
  </p>
</div>
<div id="product-table">
  <table class="main-table">
    <thead>
    <tr class="table-header">
      <th>Item #:</th>
      <th>Item Name:</th>
      <th>Quantity:</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($items as $delta => $item): ?>
      <tr class="populated-row">
        <td class="product-id-cell">
          <?php print $item['sku']; ?>
        </td>
        <td class="product-name-cell">
          <?php print $item['name']; ?>
        </td>
        <td class="quantity-cell">
          <?php print $item['quantity']; ?>
        </td>
      </tr>
    <?php endforeach; ?>
    <?php
    // If less than 15 rows, pad with empty rows.
    for ($i = 0; $i < 15 - count($items); $i++): ?>
      <tr class="empty-row">
        <td></td>
        <td></td>
        <td></td>
      </tr>
    <?php endfor; ?>
    </tbody>
  </table>
  <div class="package-info">
    <span class="package-count">
      <?php print t('Package: @delta of @count', array(
        '@delta' => $package_delta,
        '@count' => $total_packages,
      )); ?>
    </span>
  </div>
</div>
</body>
</html>
