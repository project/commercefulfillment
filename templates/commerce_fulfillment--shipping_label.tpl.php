<html>
  <head>
    <?php
/**
 * @file
 * Template for the shipping label.
 */
print $page['css']; ?>
  </head>
  <body>
    <div class="shipping-wrapper">
      <div id="shipping-header">
        <div class="shipping-company">
          <img src="<?php print $image; ?>" />
          <p class="shipping-company-info">
            <?php
              print '<strong>' . t('@company', array('@company' => $company)) . '</strong><br>'
                  . t('@address', array('@address' => $address)) . '<br>' . t('Phone: @phone', array('@phone' => $phone));
            ?>
          </p>
        </div>
        <div class="tracking-number">
          <p>
            <strong>Tracking Number: </strong>
            <?php print $tracking_number ?>
          </p>
        </div>
      </div>
      <div class="customer-shipping-info">
        <p>
        <span class="customer-label">
            <Strong>Ship To:</strong>
        </span>
          <br>
          <?php
          if(!empty($to_address)):
            print t('@first_name @last_name', array('@first_name' => $to_address['first_name'], '@last_name' => $to_address['last_name'])) . '<br>';
            print t('@ship_add, @ship_city @ship_state', array(
                '@ship_add' => $to_address['thoroughfare'],
                '@ship_city' => $to_address['locality'],
                '@ship_state' => $to_address['administrative_area'],
              )) . '<br>';

            print t('@ship_zip, @ship_country', array('@ship_zip' => $to_address['postal_code'], '@ship_country' => $to_address['country']));
          endif;
          ?>
        </p>
      </div>
    </div>
  </body>
</html>
