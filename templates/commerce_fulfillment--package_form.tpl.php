<?php
/**
 * @file
 * Template for the package form.
 */
?>
<div class="quantity-popup">Quantity: <input type="number" value=""></div>
<div class="container-creation">
  <?php print render($form['commerce_fulfillment_dnd']); ?>
  <h2>Packages:</h2>
  <?php print render($form['commerce_fulfillment_package_type']);
  print render($form['commerce_fulfillment_create']);
  print render($form['commerce_fulfillment_create_hint']); ?>
</div>
<div class="table table-unused-items">
  <div class="header">
    <div class="title">Unallocated Products</div>
  </div>
  <?php $line_item_keys = element_children($form['commerce_fulfillment_line_items']);
  if (!empty($line_item_keys)) { ?>
    <div class="description">
      <p>Drag a product into a package to add it.</p>
    </div>
  <?php } else { ?>
    <div class="description">
      <p>No products to allocate.</p>
    </div>
  <?php } ?>
  <div class="unused-items unused-items-line-items">
    <?php print render($form['commerce_fulfillment_line_items'])?>
  </div>
  <div class="clear-both"></div>
</div>
<div id="packages" class="table table-containers">
  <div class="header">
    <div class="field field-remove"><?php print render($form['commerce_fulfillment_remove_all']); ?></div>
    <div class="field field-id">Package ID</div>
    <div class="field field-type">Package Type</div>
    <div class="field field-operations">Operations</div>
    <div class="field field-line-items">Product(s)</div>
  </div>
  <?php
  $packages = isset($form['commerce_fulfillment_packages'])? $form['commerce_fulfillment_packages']: array();
  $package_keys = element_children($packages);
  if (!empty($package_keys)) {
    $first = TRUE;
    foreach ($package_keys as $key) {
      $package = $form['commerce_fulfillment_packages'][$key]; ?>
      <div id="package_<?php print $key; ?>" class="row row-container<?php ($first ? print ' first' : '') ?>" data-commercefulfillment-package-id="<?php print $package['#data-commercefulfillment-package-id'] ?>">
        <div class="field field-remove"><?php print render($package['remove_' . $key]); ?></div>
        <div class="field field-id"><?php print render($package['id']); ?></div>
        <div class="field field-type"><?php print render($package['type']); ?></div>
        <div class="field field-operations"><?php print render($package['operations']); ?></div>
        <div class="field field-line-items"><?php print render($package['line_items']); ?></div>
        <div class="clear-both"></div>
      </div>
      <?php
      $first = FALSE;
      hide($form['commerce_fulfillment_packages'][$key]);
    }
  } else { ?>
    <div class="description">No packages created.</div>
  <?php } ?>
</div>
<?php print render($form['commerce_fulfillment_delete']);
print drupal_render_children($form) ?>
