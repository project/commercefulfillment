<?php
/**
 * @file
 * Template for a line item/product.
 */
 ?>
<?php
$element = isset($element)? $element: array();
$line_item_keys = element_children($element);
if (!empty($line_item_keys)) { ?>
<div id="line-items" class="table table-items">
  <div class="header">
    <div class="field field-icon"></div>
    <div class="field field-sku">SKU</div>
    <div class="field field-title">Title</div>
    <div class="field field-unit-price">Unit Price</div>
    <div class="field field-quantity">Qty</div>
    <div class="field field-total-price">Total</div>
  </div>
  <?php
    $first = TRUE;
    foreach ($line_item_keys as $key) {
      $line_item = $element[$key]; ?>
      <div id="line_item_<?php print $key; ?>" class="row row-item-enabled<?php ($first ? print ' first' : '') ?>" data-commercefulfillment-product-id="<?php print $line_item['#data-commercefulfillment-product-id']; ?>" data-commercefulfillment-package-item-id="<?php print $line_item['#data-commercefulfillment-package-item-id']; ?>" data-commercefulfillment-package-item-quantity="<?php print $line_item['#data-commercefulfillment-package-item-quantity']; ?>">
        <div class="field field-icon"></div>
        <div class="field field-sku"><?php print render($line_item['sku']); ?></div>
        <div class="field field-title"><?php print render($line_item['title']); ?></div>
        <div class="field field-unit-price"><?php print render($line_item['unit_price']); ?></div>
        <div class="field field-quantity"><?php print render($line_item['quantity']); ?></div>
        <div class="field field-total-price"><?php print render($line_item['total_price']); ?></div>
      </div>
      <?php $first = FALSE;
    } ?>
  <div class="row-add-more row-add-more-attach">Drag items here.</div>
</div>
<?php } else { ?>
  <div class="row-add-more row-add-more-new">Drag items here.</div>
<?php } ?>
